package com.hemebiotech.analytics;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Récupère les symptômes pour calculer leurs fréquences
 */
public class TransformDataFromReader implements ISymptomTransformer {

    @Override
    public Map<String, Integer> transformData(List<String> symptoms) {
        Map<String, Integer> symptomsFrequencies = new TreeMap<>();

        for (String symptom : symptoms) {
            // Suppression des espaces à la fin de chaque élément
            String cleanedSymptom = symptom.trim();
            // Utilisation de merge pour réduire put et getOrDefault
            symptomsFrequencies.merge(cleanedSymptom, 1, Integer::sum);
        }
        return symptomsFrequencies;
    }
}
