package com.hemebiotech.analytics;

import java.util.List;

/**
 * Lire les données sur les symptômes d’une source
 * La partie importante est la valeur de retour de l’opération, qui est une
 * liste de chaînes de caractère,
 * qui peut contenir des doublons
 */
public interface ISymptomReader {
	/**
	 * Si aucune donnée n’est disponible, retourne une liste vide
	 * 
	 * @return une liste de tous les symptômes obtenus à partir d’une source de
	 *         données
	 */
	List<String> getSymptoms();
}
