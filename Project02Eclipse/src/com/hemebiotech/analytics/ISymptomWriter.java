package com.hemebiotech.analytics;

import java.util.Map;

/**
 * Récupère une liste, traite les occurrences des symptomes,
 */
public interface ISymptomWriter {
    /**
     * Si aucune donnée n’est disponible, retourne une map vide
     * 
     * @return une map de tous les symptômes ainsi que de leurs fréquences
     */
    public void writeSymptoms(Map<String, Integer> symptoms);
}
