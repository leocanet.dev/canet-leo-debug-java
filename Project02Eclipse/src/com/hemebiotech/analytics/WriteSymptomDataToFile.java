package com.hemebiotech.analytics;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

/**
 * Écris les symptômes et leurs fréquences sous forme 'clé : valeur' dans le
 * fichier result.out
 */
public class WriteSymptomDataToFile implements ISymptomWriter {

    @Override
    public void writeSymptoms(Map<String, Integer> symptoms) {

        try (FileWriter writer = new FileWriter("result.out")) {
            symptoms.forEach((symptom, quantity) -> {
                try {
                    writer.write(symptom + ": " + quantity + "\n");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
