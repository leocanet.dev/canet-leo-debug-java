package com.hemebiotech.analytics;

import java.util.List;
import java.util.Map;

/**
 * Récupère un fichier.txt, lis les symptômes
 * Transforme les symptômes pour compter leurs fréquences
 * Écris les symptômes dans result.out
 */
public class AnalyticsCounter {

    public static void main(String[] args) {

        String filePath = "Project02Eclipse/symptoms.txt";
        if (args.length != 0) {
            System.out.println("Utilisation du chemin renseigné par l'utilisateur");
            filePath = args[0];
        }

        // 1. Lire les symptômes
        ISymptomReader symptomReader = new ReadSymptomDataFromFile(filePath);
        List<String> symptoms = symptomReader.getSymptoms();

        // 2. Transformer les données
        ISymptomTransformer symptomTransformer = new TransformDataFromReader();
        Map<String, Integer> symptomsFrequencies = symptomTransformer.transformData(symptoms);

        // 3. Écrire les symptômes
        ISymptomWriter symptomWriter = new WriteSymptomDataToFile();
        symptomWriter.writeSymptoms(symptomsFrequencies);
    }
}
