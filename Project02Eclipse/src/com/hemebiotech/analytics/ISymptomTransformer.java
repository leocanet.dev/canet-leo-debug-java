package com.hemebiotech.analytics;

import java.util.List;
import java.util.Map;

/**
 * Transforme les données d'une liste en map afin de relever les occurrences
 */
public interface ISymptomTransformer {

    public Map<String, Integer> transformData(List<String> symptomsList);
}
